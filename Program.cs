﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
        }

        public static int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return tax * companyRevenue * companiesNumber / 100;
        }

        public string GetCongratulation(int input)
        {
            if (input % 2 == 0 && input >= 18)
            {
                return "Поздравляю с совершеннолетием!";
            }
            else if (input % 2 != 0 && input < 18 && input > 12)
            {
                return "Поздравляю с переходом в старшую школу!";
            }
            else
            {
                return $"Поздравляю с {input}-летием!";
            }
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            first = first.Replace(',', '.');
            second = second.Replace(',', '.');
            NumberFormatInfo numberFormatInfo = new NumberFormatInfo()
            {
                NumberDecimalSeparator = ".",
            };
            if (!Double.TryParse(first,NumberStyles.AllowDecimalPoint,numberFormatInfo, out double firstNum) || !Double.TryParse(second,NumberStyles.AllowDecimalPoint,numberFormatInfo, out double secondNum))
            {
                throw new NotImplementedException("Входной параметр не является числом");
            }
            return firstNum * secondNum;
        }

        public double GetFigureValues(FigureEnum figureType, ParameterEnum parameterToCompute, Dimensions dimensions)
        {
            switch (figureType)
            {
                case FigureEnum.Circle:
                    {
                        if (parameterToCompute == ParameterEnum.Perimeter)
                        {
                            return Math.Round(dimensions.Radius * 2 * Math.PI);
                        }
                        else if (parameterToCompute == ParameterEnum.Square)
                        {
                            return Math.Round(dimensions.Radius * dimensions.Radius * Math.PI);
                        }
                    }
                    break;
                case FigureEnum.Rectangle:
                    {
                        if (parameterToCompute == ParameterEnum.Perimeter)
                        {
                            return Math.Round(2 * (dimensions.FirstSide + dimensions.SecondSide));
                        }
                        else if (parameterToCompute == ParameterEnum.Square)
                        {
                            return Math.Round(dimensions.SecondSide * dimensions.FirstSide);
                        }
                    }
                    break;
                case FigureEnum.Triangle:
                    {
                        if (parameterToCompute == ParameterEnum.Perimeter)
                        {
                            return Math.Round(dimensions.ThirdSide + dimensions.SecondSide + dimensions.FirstSide);
                        }
                        else if (parameterToCompute == ParameterEnum.Square)
                        {
                            if (dimensions.Height > 0)
                            {
                                return Math.Round(dimensions.Height * dimensions.FirstSide * 0, 5);
                            }
                            else
                            {
                                double p = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2;
                                return Math.Round(Math.Sqrt(p * (p - dimensions.ThirdSide) * (p - dimensions.SecondSide) * (p - dimensions.ThirdSide)));
                            }
                        }
                    }
                    break;
                    
            }
            return 0;

        }
    }
}
